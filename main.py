import requests
from jinja2 import Environment, PackageLoader, select_autoescape, FileSystemLoader
import xml.etree.ElementTree as ET
from fastkml import kml
from lxml import html

env = Environment(
    #loader=PackageLoader('trip_planner', 'templates'),
    loader=FileSystemLoader('templates/'),
    autoescape=select_autoescape(['html', 'xml'])
)


class Location:
    def __init__(self, name, abstract, image_file, country=None):
        self.name = name
        self.abstract = abstract
        self.image_file = image_file
        self.country = country


def fallback_query(query):
    payload = {"q": query}
    r = requests.get("https://duckduckgo.com/html/", params=payload)
    tree = html.fromstring(r.content)
    print(tree)

    result = tree.xpath('//a[@class="result__snippet"]')[0]
    href = result.attrib['href']
    print()

def get_search_results(query):
    payload = {"format": "json", "skip_disambig": "1", "q": query}
    r = requests.get("https://api.duckduckgo.com/", params=payload)
    response = r.json()
    if not response["Results"]:
        return None
        fallback_query(query)
    else:
        abstract = response["AbstractText"]
        image_file = store_image(response["Image"])
        heading = response["Heading"]
        if response["Infobox"] != "":
            countries = [data['value'] for data in response["Infobox"]['content'] if data and 'label' in data and data['label'] == "Country"]
        else:
            countries = []
        if countries:
            country = countries[0]
        else:
            country = None
        return Location(heading, abstract, image_file, country)

def get_wikipedia_summary(query):
    payload = {"format": "json", "action": "query", "prop": "extracts", "exitnro" :"", "explaintext": "", "titles": query}
    r = requests.get("https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=Stack%20Overflow", payload)
    data = r.json()
    debug_data(data)


def store_image(image_path):
    if image_path == "":
        return
    image_url = image_path
    filename = image_url.split("/")[-1]
    img = requests.get(image_url)
    with open(filename, "wb") as imgfile:
        for chunk in img.iter_content(chunk_size=128):
            imgfile.write(chunk)
    return filename
    

def debug_data(data):
    import pprint
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(data)

with open("Balkan roadtrip.kml", 'rb') as myfile:
    doc = myfile.read()
#get_wikipedia_summary("Serbia")

locations = []
k = kml.KML()
k.from_string(doc)
features = list(k.features())
folders = features[0].features()
for folder in folders:
    print(folder.name)
    placemarks = folder.features()
    for placemark in placemarks:
        print(placemark.name)
        locations.append(get_search_results(placemark.name))

filtered = [location for location in locations if location is not None]


template = env.get_template("main.html")
with open("output.html", "w", encoding="utf-8") as html_file:
    html_file.write(template.render(locations=filtered))
